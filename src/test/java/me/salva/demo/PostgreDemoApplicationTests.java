package me.salva.demo;

import me.salva.demo.producto.ProductosService;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
@AutoConfigureMockMvc
class PostgreDemoApplicationTests {

	@MockBean
	private ProductosService productosService;

	@Test
	void contextLoads() {
	}

}
