package me.salva.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

import me.salva.demo.categoria.Categoria;
import me.salva.demo.categoria.CategoriasService;

@SpringBootTest
@AutoConfigureMockMvc
public class CategoriasTests {
    
    @MockBean
	private CategoriasService categoriasService;

    @Test
	void canCreateCategoria() {
		Categoria newCategoria = new Categoria();
		newCategoria.setId(null);
		newCategoria.setNombre("Salvador");

		Categoria createdCategoria = categoriasService.createCategoria(newCategoria);

		assertThat(createdCategoria).isNotNull();
	}

    @Test
	void productosIsNotEmpty() {
		Categoria newCategoria = new Categoria();
		newCategoria.setId(null);
		newCategoria.setNombre("Salvador");

		Categoria createdCategoria = categoriasService.createCategoria(newCategoria);

		Categoria result = categoriasService.getCategoria(createdCategoria.getId());

		assertThat(createdCategoria).isEqualTo(result);
	}

}
