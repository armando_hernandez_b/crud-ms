package me.salva.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

import me.salva.demo.producto.Producto;
import me.salva.demo.producto.ProductosService;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductosTests {

    @MockBean
	private ProductosService productosService;

    @Test
	void canCreateProducto() {
		Producto newProducto = new Producto();
		newProducto.setId(null);
		newProducto.setNombre("Salvador");

		Producto createdProducto = productosService.createProducto(newProducto);

		assertThat(createdProducto).isNotNull();
	}

	@Test
	void productosIsNotEmpty() {
		Producto newProducto = new Producto();
		newProducto.setNombre("Salvador");

		Producto createdProducto = productosService.createProducto(newProducto);

		Producto result = productosService.getProducto(createdProducto.getId());

		assertThat(createdProducto).isEqualTo(result);
	}

}
