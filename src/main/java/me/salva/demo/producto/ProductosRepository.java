package me.salva.demo.producto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ProductosRepository extends JpaRepository<Producto, Long> {
}
