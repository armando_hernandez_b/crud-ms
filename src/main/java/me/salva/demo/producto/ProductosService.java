package me.salva.demo.producto;

import java.util.List;

public interface ProductosService {
    List<Producto> getProductos();

    Producto getProducto(long id);

    Producto createProducto(Producto newProducto);

    Producto updateProducto(Producto newProducto, long id);

    void deleteProducto(long id);
}
