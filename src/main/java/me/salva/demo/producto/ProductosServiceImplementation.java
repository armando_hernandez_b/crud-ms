package me.salva.demo.producto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("productosService")
public class ProductosServiceImplementation implements ProductosService {

    public static final Logger logger = LoggerFactory.getLogger(ProductosServiceImplementation.class);

    @Autowired
    private ProductosRepository productosRepository;

    @Override
    public List<Producto> getProductos() {
        return (List<Producto>) productosRepository.findAll();
    }

    @Override
    public Producto getProducto(long id) {

        Optional<Producto> result = productosRepository.findById(id);
        if (result.isEmpty())
            return null;
        return result.get();
    }

    @Override
    public Producto createProducto(Producto newProducto) {
        logger.info(newProducto.toString());
        return productosRepository.save(newProducto);
    }

    @Override
    public Producto updateProducto(Producto newProducto, long id) {

        return productosRepository.findById(id).map(producto -> {
            producto.setNombre(newProducto.getNombre());
            return productosRepository.save(producto);
        }).orElseGet(() -> {
            newProducto.setId(id);
            return productosRepository.save(newProducto);
        });
    }

    @Override
    public void deleteProducto(long id) {
        productosRepository.deleteById(id);
    }
}
