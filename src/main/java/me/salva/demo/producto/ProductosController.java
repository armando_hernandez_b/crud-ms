package me.salva.demo.producto;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductosController {

    @Autowired
    @Qualifier("productosService")
    private ProductosService productosService;

    @Operation(summary = "Obtener lista de todos los productos", description = "No recibe ningún parámetro. " +
            "Siempre retorna la lista de todos los productos.")
    @GetMapping("/productos")
    public ResponseEntity<List<Producto>> showProductos() {
        return new ResponseEntity<>(productosService.getProductos(), HttpStatus.OK);
    }

    @Operation(summary = "Obtener el producto con el id dado", description = "Recibe un id de tipo long " +
            "correspondiente al producto esperado.")
    @GetMapping("/productos/{id}")
    public ResponseEntity<Producto> getProducto(@PathVariable long id) {
        return new ResponseEntity<>(productosService.getProducto(id), HttpStatus.FOUND);
    }

    @Operation(summary = "Crear un nuevo producto", description = "Se recibe un producto a través del body, " +
            "y este se añadirá a la base de datos. Retorna el id del nuevo producto")
    @PostMapping("/productos")
    public ResponseEntity<Producto> createProducto(@RequestBody Producto newProducto) {
        return new ResponseEntity<>(productosService.createProducto(newProducto), HttpStatus.CREATED);
    }

    @Operation(summary = "Actualizar un producto con el id dado", description = "Recibe un id de tipo long y " +
            "las propiedades a actualizar del producto")
    @PutMapping("/productos/{id}")
    public ResponseEntity<Producto> updateProducto(@RequestBody Producto newProducto, @PathVariable long id) {
        return new ResponseEntity<>(productosService.updateProducto(newProducto, id), HttpStatus.OK);
    }

    @Operation(summary = "Borrar producto con el id dado", description = "Recibe un id de tipo long, y se " +
            "elimina el producto de la base de datos con el id correspondiente")
    @DeleteMapping("/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable long id) {

        productosService.deleteProducto(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }
}
