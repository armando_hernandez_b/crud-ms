package me.salva.demo.categoria;

import java.util.List;

public interface CategoriasService {
    List<Categoria> getCategorias();

    Categoria getCategoria(long id);

    Categoria createCategoria(Categoria newCategoria);

    Categoria updateCategoria(Categoria newCategoria, long id);

    void deleteCategoria(long id);
}
