package me.salva.demo.categoria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("categoriasService")
public class CategoriasServiceImplementation implements CategoriasService {

    @Autowired
    private CategoriasRepository categoriasRepository;

    @Override
    public List<Categoria> getCategorias() {
        return (List<Categoria>) categoriasRepository.findAll();
    }

    @Override
    public Categoria getCategoria(long id) {

        Optional<Categoria> result = categoriasRepository.findById(id);
        if (result.isEmpty()) return null;
        return result.get();
    }

    public Categoria createCategoria(Categoria newCategoria) {
        return categoriasRepository.save(newCategoria);
    }

    @Override
    public Categoria updateCategoria(Categoria newCategoria, long id) {

        return categoriasRepository.findById(id).map(categoria -> {
            categoria.setNombre(newCategoria.getNombre());
            return categoriasRepository.save(categoria);
        }).orElseGet(() -> {
            newCategoria.setId(id);
            return categoriasRepository.save(newCategoria);
        });
    }

    @Override
    public void deleteCategoria(long id) {
        categoriasRepository.deleteById(id);
    }
}
