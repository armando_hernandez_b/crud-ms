package me.salva.demo.categoria;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoriasController {

    @Autowired
    @Qualifier("categoriasService")
    private CategoriasService categoriasService;

    @Operation(summary = "Obtener lista de todas las categorias", description = "No recibe ningún parámetro. " +
            "Siempre retorna la lista de todas las categorias.")
    @GetMapping("/categorias")
    public ResponseEntity<List<Categoria>> getCategorias() {
        return new ResponseEntity<>(categoriasService.getCategorias(), HttpStatus.OK);
    }

    @Operation(summary = "Obtener la categoría con el id dado", description = "Recibe un id de tipo long " +
            "correspondiente a la categoria esperada.")
    @GetMapping("/categorias/{id}")
    public ResponseEntity<Categoria> getCategoria(@PathVariable long id) {
        return new ResponseEntity<>(categoriasService.getCategoria(id), HttpStatus.FOUND);
    }

    @Operation(summary = "Crear una nueva categoría", description = "Se recibe una categoría a través del body, " +
            "y esta se añadirá a la base de datos. Retorna el id de la nueva categoría")
    @PostMapping("/categorias")
    public ResponseEntity<Categoria> createCategoria(@RequestBody Categoria newCategoria) {
        return new ResponseEntity<>(categoriasService.createCategoria(newCategoria), HttpStatus.CREATED);
    }

    @Operation(summary = "Actualizar una categoría con el id dado", description = "Recibe un id de tipo long y " +
            "las propiedas a actualizar de la categoría")
    @PutMapping("/categorias/{id}")
    public ResponseEntity<Categoria> updateCategoria(@RequestBody Categoria newCategoria, @PathVariable long id) {
        return new ResponseEntity<>(categoriasService.updateCategoria(newCategoria, id), HttpStatus.OK);
    }

    @Operation(summary = "Borrar categoría con el id dado", description = "Recibe un id de tipo long, y se " +
            "elimina la categoría de la base de datos con el id correspondiente")
    @DeleteMapping("/categorias/{id}")
    public ResponseEntity<Object> deleteCategoria(@PathVariable long id) {

        categoriasService.deleteCategoria(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }
}
